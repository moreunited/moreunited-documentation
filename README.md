# Documentation for www.moreunited.uk

Documentation for More United's main website, hosted by `readthedocs.io`

[Access documentation](https://more-united-documentation.readthedocs.io/en/latest/)

-----

All documentation files are in `docs`.

Files are built using `.rst` - [guidance](http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt)

To edit them:

1. Understand RST
2. Push to this repo.

RST is a bit of a pain to work with but once you learn it it's quite straight forward. Use [Sphinx](https://docs.readthedocs.io/en/latest/getting_started.html) to preview locally before deploying as this cuts down on time significantly.
