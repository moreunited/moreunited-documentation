.. _widget-nav:

Nav and Footer
====================================

.. list-table:: About this page type
   :widths: auto
   :header-rows: 1

   * - Parameter  
     - Value
   * - Type
     - All pages
   * - Sidebar?
     - Yes and No
   * - Tag
     - No
   * - Modifications?
     - Yes

There are two navigation elements in the Nationbuilder theme:

* The primary navigation, located under the hamburger menu and at the top of the screen
* The footer navigation

.. list-table:: Layouts available
   :widths: auto
   :header-rows: 1

   * - Nav type
     - Is checked
   * - Top nav
     - "Include in top nav"
   * - Footer
     - "Include in supporter vav"

By default, the ``page.headline`` is displayed. To override this, use tag ``Nav:``