.. _help:

How to get help
======================================

There are 4 ways you can get help:

Google it
--------------------------------------

You never know, it may turn up in the results.

Ask your colleagues
--------------------------------------

Your colleagues have experience using Nationbuilder and can help you work through basic issues

Ask Nationbuilder
--------------------------------------

If it's about using a feature or you're unsure of whether you can do ``x``, contact Nationbuilder: help@nationbuilder.com

Ask Pete
--------------------------------------

If it's about the theme, or any custom development we've done, ask Pete. Most of the team have contact details