..  _page-campaignhub:

How to prefill 123FormBuilder
====================================

Requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`123FormBuilder  <https://www.123formbuilder.com/docs/how-to-prefill-form-fields/>`_.

Guidance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. `How to prefill 123FormBuilder  <https://www.123formbuilder.com/docs/how-to-prefill-form-fields/>`_. (Note, as of 2018-06-20 this documentation has not been updated to reflect the new editor. Classic.)
2. `How to prefill into Nationbuilder emails  <https://nationbuilder.com/liquid_variables_available_in_email>`_.

To prefill content you want to use url parameters. URL parameters are everything after the `?` in a link. For example, a standard tracking link looks like this:

> www.moreunited.uk?utm_source=moreunited.uk&utm_medium=email&utm_campaign=hello

Everything after the ? is a url parameter: `utm_source=moreunited.uk&utm_medium=email&utm_campaign=hello`

Each url parameter is divided into key pairs: `utm_source=moreunited.uk` and separated by an ampersand `&`. In this example, the key is `utm_source` and the value is `moreunited.uk`

To prefill content you want to enter information into the key pairs.

A very basic example would look like this:

www.moreunited.uk/utm_source={{ recipient.email }}

For the contact example@example.com, this would render like so:

www.moreunited.uk/utm_source=example@example.com

For 123FormBuilder, things are a bit different. They have released a new form editor but not updated or built in the functionality to prefill. We cannot use their out of the box share link and need to construct our own.

This is what a working link would look like. You need to edit the values in bold

https://www.123formbuilder.com/sf.php?s=123formbuilder-**3833575**&**control41610068={{ recipient.email }}**

The `3833575` number is the ID of the survey. Find this in the URL on the edit/publish pages.

Then, you want to open up your survey in a new window and then right click the field you want to prefill. Press Inspect Element. You may need to enable Developer tab in Safari to get this...

Then, following the instructions below:

.. image:: img/123formbuilder-prefill.png

For each variable you are looking to prefill, you want to start it with `control` and end it with the id of the question, for example `control41610068`.

Example:

https://www.123formbuilder.com/sf.php?s=123formbuilder-3833575&control41610068={{ recipient.email }}

You can then iterate this:

https://www.123formbuilder.com/sf.php?s=123formbuilder-3833575&control41610068=test@test.com&control41609272=1

Once you're at this point and understand you're looking for the question id to put after the word `control`, you can go back to the 123FormBuilder documentation and follow it along to fill in the different question types. I haven't checked this too much, but if 123FormBuilder allows for question visibility to be determined by a field parameter, you could hide email marketing box for opted in users already. If not that is relatively easy to do in JS.

Useful Nationbuilder values for prefill.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* {{ recipient.first_name }}
* {{ recipient.last_name }}
* {{ recipient.email }}
* {{ recipient.is_opted_in? }}
* {% if recipient.is_opted_in? == true %}1{% else %}0{% endif %}
* {{ recipient.address.address1 }}
* {{ recipient.address.address2 }}
* {{ recipient.address.city }}
* {{ recipient.address.zip }}

Mailchimp works in a similar way, is actually easier to read. Once you pick up the above basics Mailchimp is a lot easier to understand.

`Live example <https://moreunited.nationbuilder.com/admin/broadcasters/2/mailings/2532/content>`_.
