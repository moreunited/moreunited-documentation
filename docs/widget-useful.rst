.. _widget-useful:

Useful tags
====================================

.. list-table:: Useful tags and what they do
   :widths: auto
   :header-rows: 1

   * - Tag
     - What it does
     - Detail
   * - Action:NoIndex
     - Tells search engines not to include the page in search results
     - 
   * - Nav:xxx
     - Set a custom title for navigation
     - If you want a page in the navigation (header/footer/sidebar), but the page title is too long, use this as an override.
   * - Redirect:home
     - Redirects the page to the homepage.
     - 
   * - Redirect:parent
     - Redirects to the parent of the page. If no parent, home.
     - 
   * - Redirect:external
     - Nationbuilder doesn't let us redirect to external pages. Use a basic page, and set the Content to the URL you want to go to, then tag the page.
     - Basic pages only.   
   * - 123FormBuilder:xxx or 123ContactForm:xxx
     - Embed a 123Form on a Basic page. Provide the short code (the bit before .js)
     - Basic pages only, needs testing (20/05/18)


