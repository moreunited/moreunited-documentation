..  _page-supportedprofile:
.. class:: special

Supported Profile
====================================


.. list-table:: About this page type
   :widths: auto
   :header-rows: 1

   * - Parameter	
     - Value
   * - Type
     - Basic
   * - Sidebar?
     - No
   * - Tag
     - Page:SupportedProfile
   * - Modifications to template?
     - Yes

Description
----------------------------------------

Use the Parliament profile images and append _parliament to the filename to add attribution.

The majority of the defined content is set through an extremely large tag:

ProfileInformation|5k|Hammersmith|hammersmithandy|andy4hammersmith|http://www.andyslaughter.co.uk  ProfileQuoteAndySlaughter  

Each pipebar is used by a different part of the template:

ProfileInformation - initate tag
5k - amount donated to the MP
Hammersmith| - Constituency name
hammersmithandy| - Twitter profile
andy4hammersmith| - Facebook profile
http://www.andyslaughter.co.uk - website
ProfileQuoteAndySlaughter - has a hardcoded profile quote

If you'd like to not use a particular value, leave it empty:

ProfileInformation||Hammersmith|...

Overrides
-----------------------------------

Can be set by the user

.. list-table:: Overrides (case sensitive)
   :widths: auto
   :header-rows: 1

   * - Tag
     - Will...
   * - ``hide_support_package``
     - Hides the 3-column support package block.

Profile quotes
-----------------------------------

Profile quotes are hardcoded in the file ``_t-mp_profile.html``