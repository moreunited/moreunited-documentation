..  _page-campaign:
.. class:: special

Campaigns
====================================


.. list-table:: About this page type
   :widths: auto
   :header-rows: 1

   * - Parameter	
     - Value
   * - Type
     - Various
   * - Sidebar?
     - Yes
   * - Tag
     - Various
   * - Modifications to template?
     - Yes

Description
----------------------------------------

There are four layouts available:

.. list-table:: Layouts available
   :widths: auto
   :header-rows: 1

   * - Campaign type 
     - Page Type
     - Page Tag
     - Sidebar?
     - Used for
   * - Petition
     - Petition
     - ``PageType:Petition``
     - Yes
     - Getting signatures for a campaign
   * - Vote
     - Survey
     - ``PageType:Vote``
     - Yes
     - Holding votes, such as Yes/No questions for Members
   * - Letter
     - Basic
     - ``PageType:letter``
     - Yes
     - The OrganicCampaign integration
   * - Crowdfund
     - Basic
     - ``PageType:crowdfunder``
     - Yes
     - For linking to the Crowdfunder website






The campaign layout is a 2-column layout with a main column of information and a sidebar containing a progress bar, call to action, and social buttons.

The main column is set usually by the Content field on the page. In this you can add images, video, text.

**Each Campaign requires certain tags to function. These are documented below and on-page as warning boxes.**





On top of this, large parts of the layout are customisable through tags.



You must set the Action Statement for each campaign. The Action Statement appears in the sidebar, at the top, and is the call-to-action.

Follow the on-page prompt and tag the page with ``ActionStatement:xxx``, where ``xxx`` is the statement to be displayed.



Sidebar Heading
Call to action
URL
Action Noun

However you can override this



Available layouts
-----------------------------------

.. list-table:: Functionality
   :widths: auto
   :header-rows: 1

   * - Feature 
     - Description
     - Enabled by default?
   * - Backers
     - A grid of MPs that back the campaign
     - No
   * - Progress Bar
     - A progress bar displaying the progress towards the action's goal.
     - Yes, on Petition, Letter and Crowdfund. Unavailable on Vote.
   * - Goal
     - The total number of actions we wish to hit
     - Yes, on Petition (10,000), Letter (1,000) and Crowdfund (£100,000)
   * - Blog
     - Campaign updates
     - Yes - see on-page instructions.

Petition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Required tags
   :widths: auto
   :header-rows: 1

   * - Tag
     - Required?
     - Value to set
     - What's it do
   * - PageType:Petition
     - Yes
     - 
     - 


The ``Intro`` section is used on OTHER pages as a teaser/intro to that page. It is NOT displayed on the Campaign page itself.

In the ``Intro`` section, add 1-2 sentences on what the campaign is for.

By default, Petitions have the following parameters:

* 5,000 signature goal

To set a specific signature goal, set the goal under Petition Settings > Basics

To hide the goal, tag the page with ``hide_page_target``. This will hide the progress bar and the target, but continue to show the number of actions taken.


Letter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Required tags
   :widths: auto
   :header-rows: 1

   * - Tag
     - Required?
     - Value to set
     - What's it do
     - Example
   * - OrganicID:XX
     - Yes
     - Found on the Publishing page of Organic. Should be a number, less than 20
     - Sets the target for the campaign
     - OrganicID:7
   * - ActionGoal:xxx
     - No, defaults to 1,000
     - A number, e.g. 500
     - Sets the letter-writing goal
     - ActionGoal:5000

The Letter template integrates with Organic Campaigns based on a tag that you supply.

For sorta_logged_in users, the button prefills with their information, and sets + hides the opt-in statement if they are opted in already.

By default, Letters have the following parameters:

* 1,000 letters goal
* No Organic Campaign set


Vote
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Required tags
   :widths: auto
   :header-rows: 1

   * - Tag
     - Required?
     - Value to set
     - What's it do
   * - None
     - 
     - 
     - 

The Vote template is a Nationbuilder Survey.

It does not have progress bars or social proof.

Crowdfund
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Required tags
   :widths: auto
   :header-rows: 1

   * - Tag
     - Required?
     - Value to set
     - What's it do
     - Example
   * - OrganicID:XX
     - Yes
     - Found on the Publishing page of Organic. Should be a number, less than 20
     - Sets the target for the campaign
     - OrganicID:7
   * - ActionGoal:xxx
     - No
     - A number, e.g. 1000
     - Sets the fundraising goal.
     - ActionGoal:500000

The target is £100,000.

By default, the Crowdfunder template links to https://www.crowdfunder.co.uk/more-united

You can override this with the tag ``action_url:``

e.g. ``action_url:https://monzo.com``

To be worked on:

* Integration with Nationbuilder (easy)
* Integration with the dd service - use the ``action_url``


Overrides
-----------------------------------

Most values can be overriden or set by the user.

The value to be taken will be whatever appears after the ``:``

.. list-table:: Overrides (case sensitive)
   :widths: auto
   :header-rows: 1

   * - Tag
     - Will...
   * - ``hide_page_target``
     - Hides Progress Bar, Goal
   * - ``campaignSidebarHeading:xxx``
     - Overrides the Sidebar bolded heading
   * - ``campaignButtonText``
     - Overrides the Sidebar button text
   * - ``ActionNoun:xxx``
     - Overrides the noun of the actions taken, e.g. backers, winners, etc.
   * - ``Campaign:VICTORY``
     - Sets the campaign statement
   * - ``ActionGoal:``
     - Sets the numeric goal for the campaign.
   * - ``ActionURL:``
     - Sets the URL of the target
   * - ``ActionStatement:``
     - Sets the statement above the call to action

MP-backed campaigns
-----------------------------------

Campaign pages can display the MPs that back the campaign. These are known as "backers". Backers can be MPs that are not (yet) supported by More United).

If the number of backers is greater than three, the layout may need reworking.

Adding backers is a manual process. You will need to upload names and images over and over. However, you can move backers from one page to another now.

The feature is built using the ``Featured Content Sliders`` available within the Nationbuilder interface. To add a backer, you must procde the following


.. list-table:: Requirements
   :widths: auto
   :header-rows: 1

   * - Item
     - Requirement
   * - Label
     - Appears in the CMS. Set as the name of the backer
   * - Headline
     - A two-part headline: ``{true,false}|Name of MP`` where true should be set if the MP is backed by More United. e.g. ``true|Ian Lucas``
   * - Image
     - For backed MPs, use the circle image on the mps page. For non-backed MPs, use up to 125px wide.
   * - Slug
     - The slug of the backed MP, e.g. ``ian-lucas``. If a non-backed MP, a URL won't display, and you can use anything (e.g. ``mps``)

To re-order the backers, drag within the interface, top-bottom renders left-right
