..  _page-campaignhub:

Campaign Hub
====================================


.. list-table:: About this page type
   :widths: auto
   :header-rows: 1

   * - Parameter  
     - Value
   * - Type
     - Basic
   * - Sidebar?
     - No
   * - Tag
     - ``PageType:CampaignsHub``
   * - Modifications?
     - Yes

Description
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Campaign Hub is a parent page of Campaigns. It lets you highlight a key campaign, show all active campaigns, offer a description of why More United campaigns, and show updates.  

Pages underneath the Campaign Hub are known as Campaigns.

All content displayed on the Campaign Hub is dynamically generated from elsewhere.

*Campaign Hub* is made up of 4 sections:

1. A Featured campaign
2. An array of published campaigns, arranged in a grid and sorted by date published
3. An Explainer block
4. A blog section



Featured Campaign
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Featured Campaign is the hero feature of the page. It contains a large image with rounded corners, a heading, a paragraph of subcopy, and a button.

1. Tag a Campaign with `PageType:CampaignFeatured`
2. Upload an image with the filename ending in ``_u_image``. This must be at least 800x400px.
3. If the Campaign is a ``Petition`` Page Type, then the ``Intro`` section is used from the Campaign as the subcopy. If any other Page Type is used, no subcopy is used.


Published Campaign Cards
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A grid of published campaign Cards. For the campaign to appear here as a `Card`` it must be:

1. Published
2. Tagged with ``PageType:Campaign``
3. Have an image attached with a filename ending in ``_u_image``. This should be 2:1, at least 800px wide.
4. To display a sentence underneath the headline, use tag ``CardText:``

Explainer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Tags needed
   :widths: auto
   :header-rows: 1

   *  - Page Type
      - Tag
   *  - Basic
      - ``PageType:CampaignExplainer``

Explainer content sets out why More United campaigns. Consider it Evergreen content, because your users will always want to know why you campaign.

1. Have an image attached with a filename ending in ``_u_image``. This should be 2:1, at least 800px wide.
2. Write a short first paragraph. It takes all content up to the first </p> in the Content section of the page. You can find where this is by pressing the Source button in the WYSIWYG editor on the Explainer page.

Expected content:



Blog
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Tags needed
   :widths: auto
   :header-rows: 1

   *  - Page Type
      - Tag
   *  - Blog post under ``/latest``
      - ``BlogCategory:Campaign``

Display blog posts related to campaigns. Blog posts live in */latest*.

1. Write 1-2 sentences summarising the Blog post in the *Before the Flip* section.