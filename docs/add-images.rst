.. _add-images:

How to add images
====================================

You place images in three locations:

1. The Editor
2. The social media settings
3. The attachments

Nationbuilder doesn't let us add elements to the CMS, so we need to cheat and tell the theme where to place images using the filenames that we upload.

This means the theme will look for an image added as an attachment to the page with a particular filename, e.g. "placement_hero" - it will look for the "_hero".

Resizing
----------------------------------------

You should resize and compress images before you upload them. 

You can do this in Photoshop, Indesign, or bespoke tools like ImageOptim (Mac) or RIOT (Windows).

Use JPG over PNG.

Images are lazy-loaded everywhere except when added to the WYSIWYG editor. This means images won't load until they scroll into view.

What filenames to use
----------------------------------------

.. list-table:: Appending
   :widths: auto
   :header-rows: 1

   * - Append file with	
     - Valid on page
     - What it does
     - File size?
   * - _u_image
     - Most
     - Hero image on most pages
     - Landscape, up to 1200px
   * - _blog_post_before_the_flip
     - Blog post
     - Used on the blog aggregate page, just for the top 2 posts on each page.
     - 800x400
   * - _blog_post_after_the_flip
     - Blog post
     - Appears at the top of a blog post.
     - 800x400
   * - _parliament
     - MP Profile
     - Use `Parliament's official portrait <https://beta.parliament.uk/mps>`_.
     - Portrait, 700px tall
   * - _portrait
     - MP Hub
     - Preformats to a circle.
     - Square. 150px