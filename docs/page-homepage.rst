..  page-homepage:

The homepage
====================================

.. list-table:: About this page type
   :widths: auto
   :header-rows: 1

   * - Various
     - Basic
   * - Sidebar?
     - No
   * - Tag
     - ``PageType:CampaignsHub``
   * - Modifications?
     - Yes

Components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: The homepage has 6 sections
   :widths: auto
   :header-rows: 1

   * - Section
     - Page Type
     - Location
   * - Primary Call to Action
     - Various
     - Top Level
   * - Blue Bar
     - Basic
     - required-site-components > homepage-content
   * - Homepage block top
     - Basic
     - required-site-components > homepage-content
   * - Homepage block middle
     - Basic
     - required-site-components > homepage-content
   * - Homepage block bottom
     - Basic
     - required-site-components > homepage-content
   * - Bottom Bar
     - Basic
     - required-site-components > homepage-content

Primary Call to Action
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Primary Call to Action is any one-column page layout with a call to action. This could be a Donate, Signup, or Petition page layout.

Blue Bar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Blue Bar is the page /homepage_top_info and must be tagged with ``homepage_top_info`` to function.

It is made up of two components:

.. list-table:: 
   :widths: auto
   :header-rows: 1

   * - Component
     - Description
   * - Page Headline
     - The headline.
   * - Content
     - The lead paragraph. Does not accept HTML.

Homepage blocks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Homepage can accept up to three homepage content blocks. These have a lot of settings and do not need to be changed out often.

To set the location, tag with ``homepage_content_{top,middle,bottom}``, e.g. ``homepage_content_bottom``

It is made up of two components:

.. list-table:: 
   :widths: auto
   :header-rows: 1

   * - Component
     - Description
   * - Page Headline
     - The headline.
   * - Content
     - The lead paragraph. Does not accept HTML.
   * - Image
     - A 1000x600px JPEG
   * - Page Slug
     - Set the page slug to ``slug_xxx`` to link to ``xxx``
   * - Middle Content
     - Tag with ``card_text_right ``
 

Bottom Bar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Blue Bar is the page /homepage_info_bottom and must be tagged with ``homepage_bottom_cta  `` to function.

It is made up of four components:

.. list-table:: 
   :widths: auto
   :header-rows: 1

   * - Component
     - Description
   * - Page Headline
     - The headline.
   * - Content
     - The lead paragraph. Does not accept HTML.
   * - Button Text
     - Set the button text with tag ``ButtonText:xxx``
   * - Button URL
     - Set the destination of the button using tag ``ButtonSLUG:xxx``