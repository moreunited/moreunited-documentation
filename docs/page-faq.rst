..  page-faq:

FAQs
====================================

.. list-table:: About this page type
   :widths: auto
   :header-rows: 1

   * - Various
     - FAQ
   * - Sidebar?
     - Yes
   * - Tag
     - None
   * - Modifications?
     - Some

The FAQ is made up of ``Questions`` that are slotted into a ``Section`` based on a tag. The Question must be published to appear.

.. list-table:: FAQ tags
   :widths: auto
   :header-rows: 1

   * - Section
     - Tag
   * - About
     - ``FAQ:about``
   * - Membership
     - ``FAQ:membership``
   * - Candidates
     - ``FAQ:candidates``
   * - Campaigns
     - ``FAQ:campaigns``