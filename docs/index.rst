.. moreunited documentation master file, created by
   sphinx-quickstart on Sat May 19 14:03:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the site documentation
======================================

This is the site documentation for moreunited.uk.

Documentation is being built out. If something is unclear, or missing, or wrong, create a card on Trello.

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Page Types
   :name: pagetypes

   page-*


.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Useful
   :name: widgets

   widget-*
   help

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Misc
   :name: misc

   basics*

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Tools
   :name: tools

   tools-*


About this documentation
--------------------------------------

This is hosted on readthedocs, a free platform for hosting documentation. I recommend we chuck them some money as thanks.

Repos are hosted on BitBucket.

Documentation is written in reStructuredText and parsed using Sphinx-Doc. There is a lot of guidance online on `reStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_. It is quite difficult to read/write at first.

