.. _basics-analytics:

Analytics
====================================

We deploy analytics through Google Tag Manager

The site runs 3 types of analytics:

* Google Analytics
* Facebook Analytics
* Hotjar

Google Analytics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Dimensions
   :widths: auto
   :header-rows: 1

   * - Dimension
     - What it is
     - Scope
   * - 1
     - Login status
     - User
   * - 2
     - Headline
     - Hit
   * - 3
     - PageType
     - Hit
   * - 4
     - Page
     - Hit
   * - 5
     - Parent
     - Hit
   * - 6
     - Member?
     - User
   * - 7
     - 404 Referrer
     - Hit
   * - 8
     - Opt-in status
     - User
   * - 9
     - DatePublished
     - Hit
   * - 10
     - Author
     - Hit
   * - 16
     - Theme version
     - Hit

Many components have a ``?rel`` attribute on them, which can be helpful for determining how users navigate through the site.

Ecommerce is enabled for Google Analytics

We do not run a heartbeat or scroll-depth tracking as these have not been found useful in the past.

Event tracking hasn't been looked at since Version 3 and it is likely big parts of it are broken.

Developer: Tracking events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can track interactions on elements by adding the following parameters:

1. data-analytics-category (required)
2. data-analytics-action (required)
3. data-analytics-label (optional)

If `data-analytics-label` is not present, the `page url` will be set.

Example:

> <a href="https://www.twitter.com/moreuniteduk" target="_blank" rel="noopener noreferrer" class="o-social-icon" title="Go to our Twitter" data-analytics-category="Navigation" data-analytics-action="Social"></a>

Use the following attributes to fire automatically in Google Tag Manager

.. list-table:: Attributes
   :widths: auto
   :header-rows: 1

   * - Attribute
     - Scope
     - Example
   * - data-analytics-facebook=""
     - Share links
     - <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.moreunited.uk" data-facebook-share=""></a>
   * - data-analytics-twitter=""
     - Twitter share link
     - <a href="https://twitter.com/intent/tweet?text=More United&url=https://www.moreunited.uk" data-twitter-share=""></a>




Facebook Analytics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This does the basics like pull across pageviews and commerce. Can pull across additional dimensions though...?

Hotjar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hotjar is a heatmap tool (and other things) that can be useful for identifying how users interact with a specific page.

Iona/Pete have access and it should be transferred to the main account at some point...

Account is owned by peter.martin@moreunited.org.uk