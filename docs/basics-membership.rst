.. _basics-membership:

How membership works
====================================

`Note: This may change with the introduction of GoCardless and Direct Debits`

More United membership is based on the most recent donation date.

Members are supporters that have donated in the past 365 days (1 year). A member that donates 3 times within one calendar year, one month apart, extends their membership by just two months:

* Donation #1: January 2018 ==> January 2019
* Donation #2: February 2018 ==> February 2019
* Donation #3: March 2018 ==> March 2019

How can I view members?
-------------------------------------

You can use the "Is Member" feature in People to identify Members.

To identify whether a supporter was a Founding or Original Member, you can search for ``Membership Start Date``.

Example: https://moreunited.nationbuilder.com/admin/signups?token=47a7cb3e39440712aa3a667f245ae879

Membership statuses
-------------------------------------

   * - Status
     - Definition
   * - Type
     - Various
     - Top Level
   * - Active
     - Donated in the last 365 days
   * - Lapsed
     - 3 month grace period after not donating in the last 365 days
   * - Expired
     - Not donated in the last 365 days + 3 months
   * - Cancelled
     - Actively cancelled their membership. Set manually

How is membership set?
-------------------------------------

Membership is a setting on most Nationbuilder pages. Setting this will do nothing.

When a donation is created or amended in Nationbuilder, a "webhook" is fired to a function run on Google Cloud. This function calculates the expiry date for membership and updates Nationbuilder.

This happens automatically and you don't need to do anything.
