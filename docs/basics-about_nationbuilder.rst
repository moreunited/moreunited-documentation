..  _basics-about_nationbuilder:

About our theme
====================================

The theme is based on the Nationbuilder Bootstrap (v3.x) framework.

`Bootstrap's documentation <http://getbootstrap.com/docs/3.3/>`_ is largely accurate. As expected, there are then heavy modifications to suit what we need.

This version was built by Pete Martin

Nationbuilder does not support git. This makes theme development very difficult. Instead, it uses Dropbox. Theme "backups" are kept in the Dropbox version history, and Pete does a theme backup in Google Drive on each new release.

Privacy and load time enhancements
--------------------------------------------

Nationbuilder requires that we load two blocks into the header and footer of each page. These contain useful bits of code that help the site run efficiently. They also contain code that slows down the site and reduce the privacy of our users.

This theme takes steps to reduce the load-time impact of these scripts or outright block them. As a result, there will will be at least 5 console errors. The errors usually contain ‘-’ or ‘no-tracking’.

Licensing
--------------------------------------------

Cookie Consent
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The template uses `Cookie Consent v3.0.3 <https://cookieconsent.insites.com/documentation/about-cookie-consent/>`_ MIT license.

LazyLoad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hardcoded images use `Lazy Load v.8.6.0` <http://www.andreaverlicchi.eu/lazyload/>_. Images dropped into WYSIWYG do not load lazily. MIT license.

LoadCSS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The template lazy loads non-core CSS using `LoadCSS <https://github.com/filamentgroup/loadCSS>`_. MIT license.

